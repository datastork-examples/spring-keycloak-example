package com.resource.controllers;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ResourceController {

  @GetMapping(value = "/public")
  public ResponseEntity<String> getPublicResource() {
    return ResponseEntity.ok("This is a public resource");
  }

  @GetMapping(value = "/protected")
  public ResponseEntity<String> getProtectedResource() {
    return ResponseEntity.ok("This is a protected resource");
  }

}
