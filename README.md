Resource Service
===

Follow this steps to run the application and the services it depends upon:
--

**1. Build the application**
 
``
 ./gradlew build
``

**2. Start Keycloak Docker container**

``
docker run -d  --name keycloak \
  --rm -p 8081:8080 \
  -e KEYCLOAK_USER=admin \
  -e KEYCLOAK_PASSWORD=admin \
  -e DB_VENDOR=h2 \
  jboss/keycloak
``


**3. Configure Keycloak**

``
 ./configure-keycloak.sh
``

**4. Start the application**

``
java -jar build/libs/spring-keycloak-example-1.0-SNAPSHOT.jar
``


Testing public and private endpoints
---

**1. Public:**

``
curl -X GET http://localhost:8080/public
``

Should receive a response similar to this:

``
 % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
100    25  100    25    0     0     25      0  0:00:01 --:--:--  0:00:01 25000This is a public resource
``

**2. Private without token:**

 ``
 curl -X GET http://localhost:8080/protected
 ``
 
Should receive response with status code 401 similar to this: 
 ``
   % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                  Dload  Upload   Total   Spent    Left  Speed
 100   133    0   133    0     0    133      0 --:--:-- --:--:-- --:--:--  2829{"timestamp":"2019-06-11T13:31:29.085+0000","status":401,"error":"Unauthorized","message":"No message available","path":"/protected"}
 ``
 
 **3. Private with token:**
 
 First get an Access token
 
 ``
 curl \
   -d "client_id=resource-service" \
   -d "client_secret=secret" \
   -d "username=Tom" \
   -d "password=password" \
   -d "grant_type=password" \
   "http://localhost:8081/auth/realms/spring-keycloak/protocol/openid-connect/token"
 ``
 
Replace {token} with the Access token received as response from the previous command
 
 ``
 curl -X GET \
   http://localhost:8080/protected \
   -H "Authorization: Bearer {token}"
 ``
 
Should receive a response similar to this:

``
 % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
100    28  100    28    0     0     28      0  0:00:01 --:--:--  0:00:01 28000This is a protected resource
``
 